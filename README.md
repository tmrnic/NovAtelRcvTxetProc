# NovAtel接收机文本数据解析

#### 介绍
将NovAtel接收机输出的“GPGGA”，“BESTVELA”，“RANGE”，“SATPOS”，“Ephemeris”文本数据构成为文本文件，转换成按行列排布的数据文本，方便导入Matlab分析数据

#### 软件架构
程序使用python编写，用正则表达式提取数据，用tkinter中的filedialog建立文件选择对话框，方便选择原始文本文件
程序读取文件后，按行读取数据，根据帧头选择响应的正则表达式提取GNSS输出文本中的有效信息，再输出到新的文本文件中

#### 使用说明
1.  输入文件中应当包含“BESTVELA”和“GPGGA”两种信息，且两个同时有效时，才会判定这一拍的GNSS数据有效，再将其提取输出
2.  程序仅用于数据提取，本质上时字符串的操作，没有进行数据简单处理，所以输出的速度为“水平速度+航向+天向速度”，位置信息中的经纬度为“DDMM.MMMMMM”的形式
3.  输出文件是固定文件名，当操作多组数据时，需要每次输出后将文件重命名，防止文件被覆盖